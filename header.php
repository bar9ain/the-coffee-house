<?php
if (session_status() === PHP_SESSION_NONE)
    session_start();
include "csdl.php";

if (!isset($title))
    $title = "The Coffee House";

// Tổng số lượt truy cập
$sql = "SELECT count(1) as number FROM active_sessions";
$query = $db->query($sql);
$visited = $query->fetch_assoc()["number"];

// Số người đang online
$sql = "SELECT count(1) as number FROM active_sessions WHERE logged_out = 0";
$query = $db->query($sql);
$online = $query->fetch_assoc()["number"];

// Danh mục sản phẩm
$sql = "SELECT * FROM categories";
$query = $db->query($sql);
$danhmucs = array();
while ($row = $query->fetch_assoc()) {
    $danhmucs[] = $row;
}

// Số sản phẩm trong giỏ hàng
$cart = 0;
if (isset($_SESSION["cart"])) {
    foreach ($_SESSION["cart"] as $item) {
        $cart += $item->amount;
    }
}

?>

<head>
    <title><?= $title ?></title>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="css/font-awesome.css"/>
    <link rel="stylesheet" type="text/css" href="css/style.css"/>
    <link rel="icon" href="images/logo.png">
</head>

<body>
<div class="page-header">
    <div class="page-header-wrapper">
        <form class="page-search" action="search.php" method="post">
            <input name="search" placeholder="Tìm kiếm...">
            <button type="submit"></button>
        </form>
        <?php if (isset($_SESSION["customer_id"])) { ?>
            Xin chào, <?= $_SESSION["fullname"] ?>
        <?php } ?>
    </div>
</div>
<div class="page-nav">
    <div class="wrapper">
        <div class="page-logo">
            <a href="index.php"></a>
        </div>
        <ul class="menu">
            <li><a href="index.php">Trang chủ</a></li>
            <li>
                <a>Menu</a>
                <ul class="menu-dropdown">
                    <?php foreach ($danhmucs as $row) { ?>
                        <li><a href="categories.php?dm=<?= $row["category_id"] ?>"><?= $row["category_name"] ?></a></li>
                    <?php } ?>
                </ul>
            </li>
            <li><a href="about.php">Chúng tôi</a></li>
            <li><a href="cart.php">Giỏ hàng
                    <?php if ($cart > 0) { ?>
                        (<?= $cart ?>)
                    <?php } ?>
                </a>
            </li>
            <?php if (isset($_SESSION["customer_id"])) { ?>
                <li><a href="profile.php">Hồ sơ</a></li>
                <li><a href="logout.php">Đăng xuất</a></li>
            <?php } else { ?>
                <li><a href="login.php">Đăng nhập</a></li>
                <li><a href="register.php">Đăng ký</a></li>
            <?php } ?>
        </ul>
    </div>
</div>

<?php if (isset($homepage)) { ?>
    <div class="banner">
        <div class="brand"></div>
    </div>
<?php } ?>
<div class="page-content">
