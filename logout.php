<?php
include "csdl.php";
session_start();

if (isset($_SESSION["customer_id"])) {
    $id = $_SESSION["customer_id"];
    $sql = "UPDATE active_sessions
            SET logged_out = 0
            WHERE user_id = $id";
    $db->query($sql);
}
unset($_SESSION["customer_id"]);
unset($_SESSION["fullname"]);

header("Location: login.php");
