<?php
include "header.php";
if (!isset($_SESSION["customer_id"])) {
    header("location: login.php");
}

$customer_id = $_SESSION["customer_id"];

$sql = "SELECT *
        FROM orders
        WHERE customer_id='$customer_id'";
$query = $db->query($sql);
$list = array();
while ($row = $query->fetch_array()) {
    $list[] = $row;
}
?>
<div class="user-page">
    <?php include "profile_menu.php" ?>
    <div class="user-page-content">
        <div class="user-page-header">
            <div class="user-page-header-title">Lịch sử mua đồ uống của tôi</div>
            <div class="user-page-header-subtitle">Lịch sử các đơn hàng mà bạn đã đặt</div>
        </div>
        <div class="user-page-profile">
            <div class="cart-history">
                <div class="cart-detail-row">
                    <div class="cart-detail-text"><b>Mã đơn hàng</b></div>
                    <div class="cart-detail-text"><b>Thời gian</b></div>
                    <div class="cart-detail-text"><b>Tổng tiền</b></div>
                    <div class="cart-detail-text"><b>Trạng thái</b></div>
                    <div class="cart-detail-text"><b>Chi tiết</b></div>
                </div>
                <?php foreach ($list as $item) { ?>
                    <div class="cart-detail-row">
                        <div class="cart-detail-text"><?= $item["order_id"] ?></div>
                        <div class="cart-detail-text"><?= $item["order_date"] ?></div>
                        <div class="cart-detail-text"><?= number_format($item["total_amount"]) ?>đ</div>
                        <div class="cart-detail-text">
                            <?php
                            switch ($item["status"]) {
                                case 0:
                                    echo "Mới";
                                    break;
                                case 1:
                                    echo "Đã xác nhận";
                                    break;
                                case 2:
                                    echo "Đã giao hàng";
                                    break;
                                case 3:
                                    echo "Đã nhận hàng";
                                    break;
                                case 4:
                                    echo "Đã hủy";
                                    break;
                            }
                            ?>
                        </div>
                        <div class="cart-detail-text">
                            <a href="order_detail.php?id=<?= $item["order_id"] ?>">Chi tiết</a>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>
<?php include "footer.php" ?>
