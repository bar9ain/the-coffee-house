<?php
$sql = "SELECT
            fullname,
            phone
        FROM customers
        WHERE customer_id='$customer_id'";
$query = $db->query($sql);
$row = $query->fetch_assoc();
$__hoten = $row["fullname"];
$__sdt = $row["phone"];
?>
<div class="user-page-sidebar">
    <div class="user-page-brief">
        <div class="user-page-brief-avatar">
            <img src="images/avatar.jpg">
        </div>
        <div class="user-page-brief-right">
            <div class="user-page-brief-username"><?= $__hoten ?></div>
            <div class="user-page-brief-phone"><?= $__sdt ?></div>
        </div>
    </div>
    <div class="user-page-sidebar-menu">
        <a class="user-page-sidebar-menu-entry" href="profile.php">Thông tin cá nhân</a>
        <a class="user-page-sidebar-menu-entry" href="password.php">Đổi mật khẩu</a>
        <a class="user-page-sidebar-menu-entry" href="orders.php">Lịch sử mua</a>
        <a class="user-page-sidebar-menu-entry" href="logout.php">Đăng xuất</a>
    </div>
</div>