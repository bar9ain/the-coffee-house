<?php include "header.php";

if (isset($_GET["dm"])) {
    // Get tên danh mục
    $category_id = $_GET["dm"];
    $sql = "SELECT * FROM categories WHERE category_id = '$category_id'";
    $query = $db->query($sql);
    $row = $query->fetch_assoc();
    $category_name = $row["category_name"];

    // Get danh sách danh mục
    $sql = "SELECT * FROM categories ORDER BY category_id";
    $query = $db->query($sql);
    $categories = array();
    while ($row = $query->fetch_array()) {
        $categories[] = $row;
    }

    // Get sản phẩm trong danh mục
    $sql = "SELECT
                product_id,
                product_name,
                image,
                price
            FROM products
            WHERE category_id = '$category_id'
            ORDER BY price";
    $query = $db->query($sql);
    $sanpham = array();
    while ($row = $query->fetch_array()) {
        $sanpham[] = $row;
    }
}
if (!isset($category_name)) {
    header("Location: index.php");
}

?>
    <div class="card transparent">
        <div class="category-menu">
            <?php foreach ($categories as $row) { ?>
                <a class="category-menu-item <?= $row["category_id"] == $category_id ? "active" : "" ?>"
                   href="categories.php?dm=<?= $row["category_id"] ?>">
                    <?= $row["category_name"] ?>
                </a>
            <?php } ?>
        </div>
        <div class="product-list">
            <?php foreach ($sanpham as $row) { ?>
                <div class="product">
                    <a href="product_detail.php?id=<?= $row["product_id"] ?>">
                        <img class="product-image" src="<?= $row["image"] ?>">
                        <div class="product-name"><?= $row["product_name"] ?></div>
                        <div class="product-price">
                            <?= number_format($row["price"]) ?>đ
                        </div>
                    </a>
                </div>
            <?php } ?>
        </div>
    </div>

<?php include "footer.php" ?>
