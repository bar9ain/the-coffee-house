<?php
include "header.php";
if (!isset($_SESSION["customer_id"])) {
    $alert = "Bạn phải đăng nhập để thực hiện mua hàng!";
    $fullname = $address = $phone = $email = "";
} else {
    if (!isset($_SESSION["cart"]) || $_SESSION["cart"] === null || count($_SESSION["cart"]) === 0) {
        header("location: cart.php");
    }
    $hasError = false;
    if (isset($_POST["checkout_confirm"])) {
        $order_id = time();
        $customer_id = $_POST["makhachhang"];
        $total_amount = $_POST["tongtien"];

        // Tạo đơn hàng
        $db->begin_transaction();
        $db->autocommit(false);
        $sql = "INSERT INTO orders (
                    order_id,
                    customer_id,
                    total_amount
                ) VALUES (
                    $order_id,
                    $customer_id,
                    $total_amount
                )";
        if (!$db->query($sql)) {
            $hasError = true;
            $alert = $db->error;
        }

        $cart = $_SESSION["cart"];
        foreach ($cart as $item) {
            // Insert chi tiết đơn hàng
            $sql = "INSERT INTO order_details (
                            order_id,
                            product_id,
                            quantity
                        ) VALUES (
                            '$order_id',
                            '$item->product_id',
                            '$item->amount'
                        )";
            if (!$db->query($sql)) {
                $hasError = true;
                $alert = $db->error;
                break;
            }

            $sql = "UPDATE products
                    SET sold = sold + $item->amount
                    WHERE product_id = $item->product_id";
            if (!$db->query($sql)) {
                $hasError = true;
                $alert = $db->error;
                break;
            }
        }

        // Nếu có lỗi xảy ra thì rollback dữ liệu, nếu thành công thì xóa giỏ hàng và thông báo.
        if ($hasError) {
            $db->rollback();
            include "footer.php";
            return;
        } else {
            $db->commit();
            unset($_SESSION["cart"]);
            ?>
            <div class="card transparent">
                <div class="category-name">
                    Giỏ hàng
                </div>
                <h4>Đặt hàng thành công!</h4>
            </div>
            <?php
            include "footer.php";
            return;
        }
    }

    $customer_id = $_SESSION["customer_id"];
    $sql = "SELECT
                customer_id,
                fullname,
                address,
                phone,
                email
            FROM customers
            WHERE customer_id='$customer_id'";
    $query = $db->query($sql);
    $row = $query->fetch_assoc();
    if (isset($row)) {
        $fullname = $row["fullname"];
        $address = $row["address"];
        $phone = $row["phone"];
        $email = $row["email"];
    }
}
?>

    <div class="flex-wrap">
        <div class="card half transparent flex-column">
            <div class="category-name">Giỏ hàng</div>
            <div class="cart-list">
                <?php
                $cart = $_SESSION["cart"];
                $index = 0;
                $summary = 0;
                foreach ($cart as $item) {
                    $ssql = "SELECT product_name, price, image
                             FROM products
                             WHERE product_id = '$item->product_id'";
                    $squery = $db->query($ssql);
                    $sitem = $squery->fetch_assoc();
                    ?>
                    <div class="cart-row">
                        <div class="cart-product">
                            <img alt="" src="<?= $sitem["image"] ?>">
                            <?= $sitem["product_name"] ?>
                        </div>
                        <div class="cart-quantity"><?= $item->amount ?></div>
                        <div class="cart-cost"><?= number_format($sitem["price"] * $item->amount) ?>đ</div>
                    </div>
                    <?php
                    $index++;
                    $summary += $sitem["price"] * $item->amount;
                }
                ?>
            </div>
        </div>

        <div class="card half transparent flex-column">
            <div class="category-name">Thông tin khách hàng</div>
            <div class="chitietkhachhang">
                <div class="product-detail-quantity">
                    <div class="title">Họ tên:&nbsp;</div>
                    <div class="content"><?= $fullname ?></div>
                </div>
                <div class="product-detail-quantity">
                    <div class="title">Địa chỉ:&nbsp;</div>
                    <div class="content"><?= $address ?></div>
                </div>
                <div class="product-detail-quantity">
                    <div class="title">Số điện thoại:&nbsp;</div>
                    <div class="content"><?= $phone ?></div>
                </div>
                <div class="product-detail-quantity">
                    <div class="title">Email:&nbsp;</div>
                    <div class="content"><?= $email ?></div>
                </div>
            </div>
        </div>

        <div class="card">
            <div class="checkout-footer">
                <div class="checkout-cost">
                    <div class="title">Tổng tiền:</div>
                    <div class="checkout-cost-amount"><?= number_format($summary) ?>đ</div>
                </div>
                <?php if (isset($customer_id)) { ?>
                    <form method="post">
                        <input type="hidden" name="makhachhang" value="<?= $customer_id ?>">
                        <input type="hidden" name="tongtien" value="<?= $summary ?>">
                        <button class="button" name="checkout_confirm">Xác nhận</button>
                    </form>
                <?php } ?>
            </div>
        </div>
    </div>

<?php include "footer.php" ?>