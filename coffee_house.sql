-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Máy chủ: mysql
-- Thời gian đã tạo: Th5 21, 2019 lúc 06:16 AM
-- Phiên bản máy phục vụ: 5.7.25
-- Phiên bản PHP: 7.2.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `coffee_house`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `active_sessions`
--

CREATE TABLE `active_sessions` (
  `session_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `last_login` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `logged_out` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Đang đổ dữ liệu cho bảng `active_sessions`
--

INSERT INTO `active_sessions` (`session_id`, `user_id`, `last_login`, `logged_out`) VALUES
(1, 1, '2019-05-21 14:52:53', 1);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `categories`
--

CREATE TABLE `categories` (
  `category_id` int(11) NOT NULL,
  `category_name` varchar(100) COLLATE utf8_vietnamese_ci NOT NULL,
  `description` varchar(200) COLLATE utf8_vietnamese_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_vietnamese_ci;

--
-- Đang đổ dữ liệu cho bảng `categories`
--

INSERT INTO `categories` (`category_id`, `category_name`, `description`) VALUES
(4, 'Chocolate', NULL),
(3, 'Home Made', NULL),
(2, 'Tea', NULL),
(1, 'Cafe', NULL),
(5, 'Espresso', NULL),
(6, 'Iced Blended', NULL);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `comments`
--

CREATE TABLE `comments` (
  `comment_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `rate` int(11) NOT NULL,
  `comment` varchar(500) COLLATE utf8_vietnamese_ci NOT NULL,
  `time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `approved` int(11) NOT NULL DEFAULT '0' COMMENT 'Trạng thái kiểm duyệt',
  `seen` int(11) NOT NULL DEFAULT '0',
  `reply` varchar(500) COLLATE utf8_vietnamese_ci DEFAULT NULL,
  `username` varchar(50) COLLATE utf8_vietnamese_ci DEFAULT NULL,
  `reply_time` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_vietnamese_ci;

--
-- Đang đổ dữ liệu cho bảng `comments`
--

INSERT INTO `comments` (`comment_id`, `customer_id`, `product_id`, `rate`, `comment`, `time`, `approved`, `seen`, `reply`, `username`, `reply_time`) VALUES
(1, 9, 35, 4, 'Great!!!', '2019-05-15 22:52:55', 1, 2, 'Thank you!', 'admin', '2019-05-15 22:52:55'),
(4, 1, 35, 3, 'asdasd', '2019-05-15 23:41:29', 1, 2, 'OK~', 'admin', '2019-05-15 23:49:41'),
(5, 1, 35, 5, 'Good', '2019-05-15 23:41:47', 1, 2, 'Thanks!', 'admin', '2019-05-15 23:49:19'),
(6, 1, 34, 5, 'Ngon lắm!', '2019-05-16 00:11:14', 0, 0, NULL, NULL, NULL),
(7, 1, 31, 5, 'Tuyệt vời!', '2019-05-16 14:59:41', 0, 0, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `customers`
--

CREATE TABLE `customers` (
  `customer_id` int(11) NOT NULL,
  `fullname` varchar(20) COLLATE utf8_vietnamese_ci NOT NULL,
  `address` varchar(100) COLLATE utf8_vietnamese_ci NOT NULL,
  `phone` varchar(10) COLLATE utf8_vietnamese_ci NOT NULL,
  `email` varchar(50) COLLATE utf8_vietnamese_ci NOT NULL,
  `password` varchar(50) COLLATE utf8_vietnamese_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_vietnamese_ci;

--
-- Đang đổ dữ liệu cho bảng `customers`
--

INSERT INTO `customers` (`customer_id`, `fullname`, `address`, `phone`, `email`, `password`) VALUES
(1, 'Jon Snow', '120 Phong Định Cảng, TP. Vinh, Nghệ An', '0123456789', 'jonsnow@gmail.com', '123'),
(9, 'Dani Alves', '60 Trần Phú, TP. Hà Tĩnh', '0122234578', 'alves@gmail.com', '123');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `orders`
--

CREATE TABLE `orders` (
  `order_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `order_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `total_amount` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_vietnamese_ci;

--
-- Đang đổ dữ liệu cho bảng `orders`
--

INSERT INTO `orders` (`order_id`, `customer_id`, `order_date`, `total_amount`, `status`) VALUES
(1556895986, 1, '2019-05-03 22:06:26', 90000, 0),
(1557854449, 1, '2019-05-15 00:20:49', 80000, 3),
(1557855094, 1, '2019-05-15 00:31:34', 50000, 4),
(1557986402, 1, '2019-05-16 15:00:02', 105000, 0);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `order_details`
--

CREATE TABLE `order_details` (
  `order_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_vietnamese_ci;

--
-- Đang đổ dữ liệu cho bảng `order_details`
--

INSERT INTO `order_details` (`order_id`, `product_id`, `quantity`) VALUES
(1556895986, 35, 2),
(1557854449, 31, 1),
(1557854449, 35, 1),
(1557855094, 1732315181, 10),
(1557855094, 1732315190, 1),
(1557986402, 19, 3);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `products`
--

CREATE TABLE `products` (
  `product_id` int(11) NOT NULL,
  `product_name` varchar(200) COLLATE utf8_vietnamese_ci NOT NULL,
  `price` bigint(20) NOT NULL,
  `image` text COLLATE utf8_vietnamese_ci NOT NULL,
  `category_id` int(11) NOT NULL DEFAULT '1',
  `description` text COLLATE utf8_vietnamese_ci,
  `created_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `sold` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_vietnamese_ci;

--
-- Đang đổ dữ liệu cho bảng `products`
--

INSERT INTO `products` (`product_id`, `product_name`, `price`, `image`, `category_id`, `description`, `created_time`, `sold`) VALUES
(37, 'Capuchino', 40000, 'images/products/capuchino.jpg', 5, 'Cafe hạt nguyền chất xay máy và pha máy + sữa tươi + đường(hoặc sữa đặc)', '2019-05-03 21:59:40', 0),
(36, 'Vanila Latte', 40000, 'images/products/vanilalate.jpg', 5, 'Cafe hạt nguyền chất xay máy và pha máy + đường + sizo vanila', '2019-05-03 21:59:40', 0),
(35, 'Mocha Cốm', 45000, 'images/products/mochacom.jpg', 4, 'Bột chocolate/sữa đặc/bột mix/sữa tươi/cốm tươi', '2019-05-03 21:59:40', 3),
(34, 'Mocha Kem Tươi', 45000, 'images/products/mochakem.jpg', 4, 'Bột chocolate/sữa đặc/bột mix/sữa tươi/kem tươi', '2019-05-03 21:59:40', 0),
(33, 'Mocha Chocolate', 45000, 'images/products/mocha.jpg', 4, 'Bột chocolate/sữa đặc/bột mix/sữa tươi', '2019-05-03 21:59:40', 0),
(32, 'Nước Dừa Tươi', 40000, 'images/products/nuocdua.jpg', 3, 'Dừa quả tươi', '2019-05-03 21:59:40', 0),
(31, 'Nước Ép Dứa', 35000, 'images/products/epdua.jpg', 3, 'Dứa quả tươi', '2019-05-03 21:59:40', 1),
(30, 'Nước Ép Ổi', 35000, 'images/products/epoi.jpg', 3, 'Ổi quả tươi', '2019-05-03 21:59:40', 0),
(29, 'Nước Ép Cà Rốt', 35000, 'images/products/epcarot.jpg', 3, 'Củ cà rốt tươi', '2019-05-03 21:59:40', 0),
(28, 'Nước Ép Dưa Hấu', 35000, 'images/products/duahau.jpg', 3, 'Dưa hấu tươi quả', '2019-05-03 21:59:40', 0),
(27, 'Nước Cam Vắt', 35000, 'images/products/nuoccam.jpg', 3, 'Cam tươi quả', '2019-05-03 21:59:40', 0),
(26, 'Nước Cốt Sấu', 25000, 'images/products/xau.jpg', 3, 'Quả xấu tươi ngâm cốt đường', '2019-05-03 21:59:40', 0),
(25, 'Mơ Quất Nha Đam', 35000, 'images/products/moquat.jpg', 3, 'Mơ ngâm/Nước Quất/nha đam Nước', '2019-05-03 21:59:40', 0),
(24, 'Trà Gừng', 15000, 'images/products/gung.jpg', 2, 'Trà gừng Mật ong(hoặc gừng tươi)', '2019-05-03 21:59:40', 0),
(23, 'Trà Lipton', 15000, 'images/products/lipton.jpg', 2, 'Trà Lipton', '2019-05-03 21:59:40', 0),
(22, 'Trà Vải', 35000, 'images/products/vaithieu.jpg', 2, 'Nước cốt Vải thiều ngâm', '2019-05-03 21:59:40', 0),
(21, 'Cafe Cốt Dừa', 30000, 'images/products/cotdua.jpg', 1, 'Cafe hạt nguyên chất pha sẵn + Nước cốt dừa', '2019-05-03 21:59:40', 0),
(20, 'Bạc Xỉu', 25000, 'images/products/bacxiu.jpg', 1, 'Cafe hạt nguyên chất + sữa đặc + sữa tươi', '2019-05-03 21:59:40', 0),
(19, 'Sữa Chua Đánh Socola', 35000, 'images/products/suachuasocola.jpg', 6, 'Sữa chua hộp/sữa đặc/đường/sốt socola', '2019-05-03 21:59:40', 3),
(18, 'Sữa Chua Đánh Cafe', 35000, 'images/products/suachuacafe.jpg', 6, 'Sữa chua hộp/sữa đặc/đường/cafe', '2019-05-03 21:59:40', 0),
(17, 'Sữa Chua Đánh Đá', 35000, 'images/products/suachuadanhda.jpg', 6, 'Sữa chua hộp/Nước cốt chanh/sữa đặc/đường', '2019-05-03 21:59:40', 0),
(16, 'Americano', 40000, 'images/products/americano.jpg', 5, 'Cafe hạt nguyền chất xay máy và pha máy + sữa tươi + đường', '2019-05-03 21:59:40', 0),
(15, 'Espresso Nâu', 35000, 'images/products/epressonau.jpg', 5, 'Cafe hạt nguyền chất xay máy và pha máy + sữa đặc', '2019-05-03 21:59:40', 0),
(14, 'Espresso Đen', 30000, 'images/products/epressoden.jpg', 5, 'Cafe hạt nguyền chất xay máy và pha máy + đường', '2019-05-03 21:59:40', 0),
(13, 'Chocolate Rượu Rum', 40000, 'images/products/chocolateruou.jpg', 4, 'Bột chocolate và rượu rum', '2019-05-03 21:59:40', 0),
(12, 'Chocolate Hạt Dẻ', 35000, 'images/products/chocolatehatde.jpg', 4, 'Bột chocolate và sizo hạt dẻ', '2019-05-03 21:59:40', 0),
(11, 'Chocolate Quế', 35000, 'images/products/chocolateque.jpg', 4, 'Bột chocolate và sizo quế', '2019-05-03 21:59:40', 0),
(9, 'Chanh Leo Vải', 35000, 'images/products/chanhleo.jpg', 3, 'Chanh leo tươi và sizo Vải ', '2019-05-03 21:59:40', 0),
(10, 'Chanh Quất Mật Ong', 35000, 'images/products/chanhmatong.jpg', 3, 'Chanh tươi/Nước Quất/Mật ong', '2019-05-03 21:59:40', 0),
(8, 'Nước Chanh Tươi', 30000, 'images/products/nuocchanh.jpg', 3, 'Chanh tươi nguyên chất', '2019-05-03 21:59:40', 0),
(7, 'Trà Tắc', 35000, 'images/products/tratac.jpg', 2, 'Nước cốt tắc', '2019-05-03 21:59:40', 0),
(5, 'Trà Đào Cam Sả', 35000, 'images/products/camxa.jpg', 2, 'Trà đào và Nước cốt cam', '2019-05-03 21:59:40', 0),
(6, 'Trà Đào Chanh Sả', 35000, 'images/products/chanhxa.jpg', 2, 'Trà đào và Nước cốt chanh tươi', '2019-05-03 21:59:40', 0),
(4, 'Nâu SG', 15000, 'images/products/nausg.jpg', 1, 'Cafe hạt nguyên chất pha sẵn + sữa đặc', '2019-05-03 21:59:40', 0),
(3, 'Đen SG', 15000, 'images/products/densg.jpg', 1, 'Cafe hạt nguyên chất pha sẵn + đường', '2019-05-03 21:59:40', 0),
(2, 'Nâu phin', 15000, 'images/products/nauphin.jpg', 1, 'Cafe hạt nguyên chất ủ phin + sữa đặc', '2019-05-03 21:59:40', 0),
(1, 'Đen phin', 15000, 'images/products/denphin.jpg', 1, 'Cafe hạt nguyên chất ủ phin + đường', '2019-05-03 21:59:40', 0),
(38, 'Sữa Chua Đánh Chanh Leo', 35000, 'images/products/suachuachanhleo.jpg', 6, 'Sữa chua hộp/sữa đặc/đường/Nước cốt chanh leo', '2019-05-03 21:59:40', 0),
(39, 'Sữa Chua Đánh Việt Quất', 35000, 'images/products/suachuavietquat.jpg', 6, 'Sữa chua hộp/sữa đặc/đường/sinh tố bốn mùa việt Quất', '2019-05-03 21:59:40', 0),
(40, 'Cookies', 35000, 'images/products/cookies.jpg', 6, 'Bột socola/bột mix/sữa đặc/bánh quy đen ', '2019-05-03 21:59:40', 0),
(41, 'Matcha Trà Xanh', 35000, 'images/products/matcha.jpg', 6, 'Bột trà xanh/sữa đặc/bột mix/sữa tươi', '2019-05-03 21:59:40', 0),
(42, 'Espresso Kem', 40000, 'images/products/epressokem.jpg', 5, 'Cafe hạt nguyền chất xay máy và pha máy + kem tươi tự làm', '2019-05-03 21:59:40', 0),
(43, 'Nước Ép Cóc', 35000, 'images/products/epcoc.jpg', 3, 'Từ những quả cóc tươi ngon nhất', '2019-05-03 21:59:40', 0);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `users`
--

CREATE TABLE `users` (
  `username` varchar(20) COLLATE utf8_vietnamese_ci NOT NULL,
  `password` varchar(20) COLLATE utf8_vietnamese_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_vietnamese_ci;

--
-- Đang đổ dữ liệu cho bảng `users`
--

INSERT INTO `users` (`username`, `password`) VALUES
('admin', 'admin'),
('admin2', '1'),
('root', '1');

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `active_sessions`
--
ALTER TABLE `active_sessions`
  ADD PRIMARY KEY (`session_id`);

--
-- Chỉ mục cho bảng `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`category_id`);

--
-- Chỉ mục cho bảng `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`comment_id`);

--
-- Chỉ mục cho bảng `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`customer_id`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `sdt` (`phone`);

--
-- Chỉ mục cho bảng `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`order_id`);

--
-- Chỉ mục cho bảng `order_details`
--
ALTER TABLE `order_details`
  ADD PRIMARY KEY (`order_id`,`product_id`) USING BTREE;

--
-- Chỉ mục cho bảng `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`product_id`);

--
-- Chỉ mục cho bảng `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`username`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `active_sessions`
--
ALTER TABLE `active_sessions`
  MODIFY `session_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT cho bảng `categories`
--
ALTER TABLE `categories`
  MODIFY `category_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT cho bảng `comments`
--
ALTER TABLE `comments`
  MODIFY `comment_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT cho bảng `customers`
--
ALTER TABLE `customers`
  MODIFY `customer_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT cho bảng `orders`
--
ALTER TABLE `orders`
  MODIFY `order_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1557986403;

--
-- AUTO_INCREMENT cho bảng `products`
--
ALTER TABLE `products`
  MODIFY `product_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1732315192;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
