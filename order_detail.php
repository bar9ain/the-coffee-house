<?php
include "header.php";
if (!isset($_SESSION["customer_id"])) {
    header("location: login.php");
}

$customer_id = $_SESSION["customer_id"];

if (isset($_GET["id"])) {
    $order_id = $_GET["id"];
    $sql = "SELECT *
            FROM orders
            WHERE order_id = '$order_id'
            AND customer_id = '$customer_id'";
    $result = $db->query($sql);
    $row = $result->fetch_assoc();
    if ($row == null) {
        header("Location: orders.php");
    } else {
        $order_id = $row["order_id"];
        $order_date = $row["order_date"];
        $total_amount = $row["total_amount"];
        $status = $row["status"];
    }
    $sql = "SELECT
                products.product_id,
                products.image,
                products.product_name,
                products.price,
                order_details.quantity
            FROM order_details
            LEFT JOIN products ON order_details.product_id = products.product_id
            WHERE order_id = '$order_id'";
    $result = $db->query($sql);
    $list = array();
    while ($i = $result->fetch_array()) {
        $list[] = $i;
    }
} else {
    header("Location: orders.php");
}

?>
<div class="user-page">
    <?php include "profile_menu.php" ?>
    <div class="user-page-content">
        <div class="user-page-header">
            <div class="user-page-header-title">Chi tiết đơn hàng</div>
            <div class="user-page-header-subtitle">Mã đơn hàng: <?= $order_id ?>
                | Thời gian: <?= $order_date ?></div>
        </div>
        <div class="edit-body">
            <div class="edit-body-cart">
                <?php foreach ($list as $item) { ?>
                    <div class="cart-detail-row">
                        <div class="cart-detail-image">
                            <img src="<?= $item["image"] ?>">
                        </div>
                        <div class="cart-detail-text">
                            <div class="cart-detail-title"><?= $item["product_name"] ?></div>
                            <div class="cart-detail-quantity">x<?= $item["quantity"] ?></div>
                        </div>
                        <div class="cart-detail-price"><?= number_format($item["price"]) ?>đ</div>
                    </div>
                <?php } ?>
                <div class="cart-detail-row cart-detail-summary">
                    <div class="cart-detail-summary-label">Tổng tiền hàng</div>
                    <div class="cart-detail-summary-value">
                        <span><?= number_format($total_amount) ?>đ</span>
                    </div>
                </div>
                <div class="cart-detail-row cart-detail-summary">
                    <div class="cart-detail-summary-label">Trạng thái</div>
                    <div class="cart-detail-summary-value">
                        <?php
                        switch ($status) {
                            case 0:
                                echo "Chưa duyệt";
                                break;
                            case 1:
                                echo "Đã xác nhận";
                                break;
                            case 2:
                                echo "Đã giao hàng";
                                break;
                            case 3:
                                echo "Đã nhận hàng";
                                break;
                            case 4:
                                echo "Đã hủy";
                                break;
                        }
                        ?>
                    </div>
                </div>
                <div class="edit-submit">
                    <a href="orders.php" class="btn-cancel">Trở lại</a>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include "footer.php" ?>
