<?php include "header.php" ?>

</div>

<div class="about-banner">
    <p>The Coffee House<br>85 Nguyễn Văn Cừ, TP. Vinh</p>
</div>
<div class="about-map">
    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3779.7369711012393!2d105.68483881538083!3d18.675795569281615!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3139cf6f287427d1%3A0xa8a133a5d5b2f955!2zVGhlIENvZmZlZSBIb3VzZSAtIE5ndXnhu4VuIFbEg24gQ-G7qyAoTmdo4buHIEFuKQ!5e0!3m2!1sen!2s!4v1557984816020!5m2!1sen!2s"
            width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
</div>
<section class="about">
    <div class="page-content">
        <div class="about-header">HÔM NAY CÓ GÌ?</div>
        <div class="about-body">
            <div class="about-row">
                <div class="about-image">
                    <img src="images/c1.jpg">
                </div>
                <div class="about-paragraph">
                    <div class="about-title">GHÉ "NHÀ" NGHE KỂ CHUYỆN CÀ PHÊ</div>
                    <div class="about-content">Có thể chỉ mất 30 giây để chuẩn bị một ly cà phê, nhưng để cây cà phê ra
                        trái là thành quả 3 năm của rất nhiều người: những người nông dân, thợ rang, pha chế và những
                        thành viên khác tại Nhà. Ly cà phê The Coffee House gửi đến bạn là một bức thư kể về hành trình
                        của những hạt cà phê, giản dị mà đầy cảm hứng.
                    </div>
                </div>
            </div>
            <div class="about-row">
                <div class="about-paragraph">
                    <div class="about-title">LỜI MỞ ĐẦU CHO MUÔN VÀN SẺ CHIA</div>
                    <div class="about-content">Uống cà phê đâu phải chỉ xoay quanh chuyện chiếc phin và những gì bên
                        trong nó, mà còn là câu chuyện bên ngoài và xung quanh chiếc phin.<br>
                        <b>"Mình cà phê nhé"</b>, âu cũng là cách mở đầu những tâm tình, những câu chuyện sẻ chia bên
                        tách cà phê với những người bạn cùng bàn.
                    </div>
                </div>
                <div class="about-image">
                    <img src="images/c2.jpg">
                </div>
            </div>
            <div class="about-row">
                <div class="about-image">
                    <img src="images/c3.jpg">
                </div>
                <div class="about-paragraph">
                    <div class="about-title">AI CŨNG CÓ MỘT THE COFFEE HOUSE ĐỂ TRỞ VỀ</div>
                    <div class="about-content">Nếu một ngày kia có điều gì làm bạn mỏi mệt quá, hãy nhớ The Coffee House
                        - với không gian ấm cúng, những tách cà phê thơm nồng và những người bạn "cùng nhà" ân cần, niềm
                        nở - là nơi mà bạn có thể trở về và tạm bỏ đi những bộn bề đằng sau cánh cửa.
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php include "footer.php" ?>
