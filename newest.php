<?php include "header.php";

$sql = "SELECT
            product_id,
            product_name,
            image,
            price
        FROM products
        ORDER BY created_time DESC
        LIMIT 0, 24";
$query = $db->query($sql);
$sanpham = array();
while ($row = $query->fetch_array()) {
    $sanpham[] = $row;
}

?>
    <div class="card transparent">
        <div class="category-name">Sản phẩm mới</div>
        <?php foreach ($sanpham as $row) { ?>
            <div class="product">
                <a href="product_detail.php?id=<?= $row["product_id"] ?>">
                    <img class="product-image" src="<?= $row["image"] ?>">
                    <div class="product-name"><?= $row["product_name"] ?></div>
                    <div class="product-price">
                        <?= number_format($row["price"], 0, 3, '.') ?>₫
                    </div>
                </a>
            </div>
        <?php } ?>
    </div>

<?php include "footer.php" ?>