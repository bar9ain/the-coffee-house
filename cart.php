<?php
session_start();

if (isset($_POST["save"])) {
    if (isset($_SESSION["cart"])) {
        $cart = $_SESSION["cart"];
        foreach ($_POST["quantity"] as $index => $value) {
            $cart[$index]->amount = $value;
        }
        $_SESSION["cart"] = $cart;
    }
}

include "header.php";

if (isset($_POST["cart_submit"])) {
    if (isset($_SESSION["customer_id"])) {
        header("location: checkout.php");
    } else {
        $alert = "Bạn phải đăng nhập để thực hiện mua hàng!";
    }
}

if (isset($_GET["delete"]) && isset($_SESSION["cart"])) {
    $cart = $_SESSION["cart"];
    $index = $_GET["delete"];
    unset($cart[$index]);
    $_SESSION["cart"] = array_values($cart);
    header("location: cart.php");
}

if (!isset($_SESSION["cart"]) || $_SESSION["cart"] === null || count($_SESSION["cart"]) === 0) { ?>
    <div class="card transparent">
        <div class="category-name">
            Giỏ hàng
        </div>
        <h4>Chưa có sản phẩm nào trong giỏ hàng</h4>
    </div>
<?php } else { ?>

    <form method="post">
        <div class="card">
            <div class="cart-header">
                <div class="cart-product">Sản phẩm</div>
                <div class="cart-price">Đơn giá</div>
                <div class="cart-quantity">Số lượng</div>
                <div class="cart-cost">Số tiền</div>
                <div class="cart-action">Thao tác</div>
            </div>
        </div>

        <div class="card">
            <div class="cart-list">
                <?php
                $cart = $_SESSION["cart"];
                $index = 0;
                $summary = 0;
                foreach ($cart as $item) {
                    $ssql = "SELECT product_name, price, image
                             FROM products
                             WHERE product_id = '$item->product_id'";
                    $squery = $db->query($ssql);
                    $sitem = $squery->fetch_assoc();
                    ?>
                    <div class="cart-row">
                        <div class="cart-product">
                            <img alt="" src="<?= $sitem["image"] ?>">
                            <?= $sitem["product_name"] ?>
                        </div>
                        <div class="cart-price"><?= number_format($sitem["price"]) ?>đ</div>
                        <div class="cart-quantity">
                            <input type="number" min="1" name="quantity[]" value="<?= $item->amount ?>"
                                   style="width: 50px; text-align: center"></div>
                        <div class="cart-cost"><?= number_format($sitem["price"] * $item->amount) ?>đ</div>
                        <div class="cart-action"><a href="cart.php?delete=<?= $index ?>">Xóa</a></div>
                    </div>
                    <?php
                    $index++;
                    $summary += $sitem["price"] * $item->amount;
                }
                ?>
            </div>
        </div>

        <div class="card">
            <div class="cart-footer">
                <div class="cart-cost">
                    <div class="title">Tổng tiền:</div>
                    <div class="checkout-cost-amount"><?= number_format($summary) ?>đ</div>
                </div>
                <button class="button" name="save">Lưu thay đổi</button>
                <button class="button" name="cart_submit" style="margin-left: 10px">Thanh toán</button>
            </div>
        </div>
    </form>
<?php } ?>
<?php include "footer.php" ?>
