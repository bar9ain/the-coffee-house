<?php include "header.php";

if (isset($_POST["forgot-password"])) {
    $email = $_POST["email"];
    $sdt = $_POST["sdt"];
    $sql = "SELECT customer_id
            FROM customers
            WHERE email='$email'
            AND phone='$sdt'";
    $check = $db->query($sql);
    if ($check->num_rows > 0) {
        $row = $check->fetch_assoc();
        $customer_id = $row["customer_id"];
        $sql = "UPDATE customers SET password = '123' WHERE customer_id = '$customer_id'";
        if ($db->query($sql)) {
            $alert = "Mật khẩu của bạn đã được đặt lại thành 123";
            $redirect = "login.php";
        } else {
            $alert = $db->error;
        }
    } else {
        $authen_error = true;
    }
}

?>
    <form method="post">
        <div class="authen-modal">
            <div class="authen-header">
                <a class="authen-header-tab active">Quên mật khẩu</a>
            </div>
            <div class="authen-body">
                <p>Vui lòng nhập chính xác email và số điện thoại để đặt lại mật khẩu!</p>
                <div class="input-with-status">
                    <input class="input-with-status-input"
                           type="email"
                           name="email"
                           placeholder="Email"
                           autofocus
                           required>
                </div>
                <div class="input-with-status">
                    <input class="input-with-status-input"
                           type="text"
                           name="sdt"
                           placeholder="Số điện thoại"
                           required>
                </div>
                <?php if (isset($authen_error)) { ?>
                    <div class="authen-error">Thông tin bạn nhập vào không đúng</div>
                <?php } ?>
            </div>
            <div class="authen-footer">
                <a href="login.php" class="btn-cancel">Trở Lại</a>
                <button type="submit" name="forgot-password" class="button">
                    Đặt lại mật khẩu
                </button>
            </div>
        </div>
    </form>
<?php include "footer.php" ?>