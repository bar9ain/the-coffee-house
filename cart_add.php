<?php
session_start();
if (isset($_POST["order"])) {
    $cart = isset($_SESSION["cart"]) ? $_SESSION["cart"] : array();
    $existed = false;
    foreach ($cart as $item) {
        if ($item->product_id === $_POST["order"]) {
            $item->amount += $_POST["amount"];
            $existed = true;
            break;
        }
    }
    if (!$existed) {
        $cart[] = (object)[
            "product_id" => $_POST["order"],
            "amount" => $_POST["amount"]
        ];
    }
    $_SESSION["cart"] = $cart;
    header("location: cart.php");
}