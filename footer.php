</div>
<div class="page-footer">
    <div class="wrapper">
        <div class="page-footer-column">
            <h4>The Coffee House</h4>
            <p>Địa chỉ: 85 Nguyễn Văn Cừ, TP. Vinh, Nghệ An</p>
            <p>Điện thoại: 0123 456 789</p>
            <p>Email: the_coffee_house@gmail.com</p>
            <p>Số lượt truy cập: <?= $visited ?></p>
            <p>Số người đang online: <?= $online ?></p>
        </div>
        <div class="page-footer-column">
            <h4>The Coffee House</h4>
            <?php foreach ($danhmucs as $row) { ?>
                <p><a href="categories.php?dm=<?= $row["category_id"] ?>"><?= $row["category_name"] ?></a></p>
            <?php } ?>
        </div>
        <div class="page-footer-column">
            <img src="images/facebook.png">
        </div>
    </div>
</div>
<?php
if (isset($alert)) {
    echo "<script>";
    echo "alert('$alert');";
    echo "</script>";
}
if (isset($redirect)) {
    echo "<script>";
    echo "window.location.href = '$redirect'";
    echo "</script>";
}
?>
</body>
</html>
