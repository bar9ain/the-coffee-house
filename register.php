<?php include "header.php";

if (isset($_POST["register"])) {
    $email = $_POST["email"];
    $phone = $_POST["sdt"];
    $fullname = $_POST["hoten"];
    $address = $_POST["diachi"];
    $password = $_POST["matkhau"];
    $password2 = $_POST["matkhau2"];
    if ($password !== $password2) {
        $register_error = "Mật khẩu không khớp!";
    } else {
        $sql = "INSERT INTO customers(
                    fullname,
                    address,
                    phone,
                    email,
                    password
                ) VALUES (
                    '$fullname',
                    '$address',
                    '$phone',
                    '$email',
                    '$password'
                )";
        if ($db->query($sql)) {
            header("location: login.php");
        } else {
            if (strpos($db->error, "email")) {
                $register_error = "Email này đã được đăng ký.";
            } else if (strpos($db->error, "sdt")) {
                $register_error = "Số điện thoại này đã được đăng ký.";
            }
        }
    }
}

?>
    <form method="post" autocomplete="off">
        <div class="authen-modal">
            <div class="authen-header">
                <a class="authen-header-tab active">Đăng ký</a>
            </div>
            <div class="authen-body">
                <div class="input-with-status">
                    <input class="input-with-status-input"
                           type="email"
                           name="email"
                           placeholder="Email"
                           autofocus
                           required>
                </div>
                <div class="input-with-status">
                    <input class="input-with-status-input"
                           type="text"
                           name="sdt"
                           placeholder="Số điện thoại"
                           required>
                </div>
                <div class="input-with-status">
                    <input class="input-with-status-input"
                           type="text"
                           name="hoten"
                           placeholder="Họ tên"
                           required>
                </div>
                <div class="input-with-status">
                    <input class="input-with-status-input"
                           type="text"
                           name="diachi"
                           placeholder="Địa chỉ"
                           required>
                </div>
                <div class="input-with-status">
                    <input class="input-with-status-input"
                           type="password"
                           name="matkhau"
                           placeholder="Mật khẩu"
                           required>
                </div>
                <div class="input-with-status">
                    <input class="input-with-status-input"
                           type="password"
                           name="matkhau2"
                           placeholder="Nhập lại mật khẩu"
                           required>
                </div>
                <?php if (isset($register_error)) { ?>
                    <div class="authen-error"><?= $register_error ?></div>
                <?php } ?>
            </div>
            <div class="authen-footer">
                <a href="login.php" class="btn-cancel">Trở Lại</a>
                <button type="submit" name="register" class="button">
                    Đăng ký
                </button>
            </div>
        </div>
    </form>
<?php include "footer.php" ?>