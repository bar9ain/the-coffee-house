<?php
$title = "Chi tiết sản phẩm";
include "cart_add.php";
include "header.php";


if (isset($_GET["id"])) {
    // Nội dung sản phẩm
    $product_id = $_GET["id"];

    // Cập nhật trạng thái đã xem
    if (isset($_GET["comment_id"])) {
        $comment_id = $_GET["comment_id"];
        $sql = "UPDATE comments SET seen=1 WHERE comment_id='$comment_id' AND seen=0";
        if ($db->query($sql)) {
            header("Location: product_detail.php?id=$product_id#comment_$comment_id");
        }
    }

    // Gửi comment và rate
    if (isset($_POST["comment"])) {
        $customer_id = $_SESSION["customer_id"];
        $comment = $_POST["comment"];
        $rating = $_POST["rating"];
        $sql = "INSERT INTO comments(customer_id, product_id, rate, comment)
                VALUES ('$customer_id', '$product_id', '$rating', '$comment')";
        if ($db->query($sql)) {
            header("Location: product_detail.php?id=$product_id");
        }
    }

    // Trả lời comment
    if (isset($_POST["reply"])) {
        $comment_id = $_POST["reply"];
        $username = $_SESSION["admin"];
        $reply = $_POST["reply_text"];
        $sql = "UPDATE comments
                SET reply = '$reply', reply_time = CURRENT_TIMESTAMP, username = '$username', seen = 2
                WHERE comment_id = '$comment_id'";
        if ($db->query($sql)) {
            header("Location: product_detail.php?id=$product_id#comment_$comment_id");
        }
    }

    $sql = "SELECT * FROM products WHERE product_id = '$product_id'";
    $query = $db->query($sql);
    $item = $query->fetch_assoc();
    if ($item === null)
        header("Location: index.php");

    // Sản phẩm khác
    $sql = "SELECT
                product_id,
                product_name,
                image,
                price
            FROM products
            WHERE product_id != $product_id
            ORDER BY sold DESC
            LIMIT 0, 4";
    $query = $db->query($sql);
    $sanphamkhac = array();
    while ($row = $query->fetch_array())
        $sanphamkhac[] = $row;

    // Danh sách bình luận
    $sql = "select * from comments
            left join customers on comments.customer_id = customers.customer_id
            where product_id = $product_id and approved = 1
            order by comments.time desc";
    $query = $db->query($sql);
    $comments = array();
    while ($row = $query->fetch_array())
        $comments[] = $row;
} else {
    header("Location: index.php");
}
?>

    <form method="post">
        <div class="card">
            <div class="product-detail-image">
                <img width="150" src="<?= $item["image"] ?>">
            </div>
            <div class="product-details">
                <div class="product-detail-title"><?= $item["product_name"] ?></div>
                <div class="product-detail-price">
                    <?= number_format($item["price"]) ?>đ
                </div>
                <div class="product-detail-description">
                    <p><b>Thành phần:</b></p>
                    <?= $item["description"] ?>
                </div>
                <div class="product-detail-quantity">
                    <div class="product-detail-quantity-label">Số lượng</div>
                    <div class="product-detail-quantity-input-outer">
                        <div class="product-detail-quantity-input">
                            <input class="input-outline" type="number" value="1" min="1" name="amount">
                        </div>
                    </div>
                </div>
                <div class="product-detail-order">
                    <button class="button" name="order" value="<?= $item["product_id"] ?>">Mua ngay</button>
                </div>
            </div>
        </div>
    </form>

    <div class="card transparent">
        <div class="category-name">Đánh giá sản phẩm</div>
        <?php if (isset($_SESSION["customer_id"])) { ?>
            <div class="comment-box">
                <form method="post">
                    <textarea name="comment" rows="5" placeholder="Bình luận với tên <?= $_SESSION["fullname"] ?>"
                              required></textarea>
                    <div class="rating">
                        <input type="radio" id="star5" name="rating" value="5" required/>
                        <label class="full" for="star5"></label>
                        <input type="radio" id="star4" name="rating" value="4" required/>
                        <label class="full" for="star4"></label>
                        <input type="radio" id="star3" name="rating" value="3" required/>
                        <label class="full" for="star3"></label>
                        <input type="radio" id="star2" name="rating" value="2" required/>
                        <label class="full" for="star2"></label>
                        <input type="radio" id="star1" name="rating" value="1" required/>
                        <label class="full" for="star1"></label>
                    </div>
                    <button class="button">Gửi đánh giá</button>
                </form>
            </div>
        <?php } else { ?>
            <div class="comment">
                <div class="comment-text"><a href="login.php">Đăng nhập</a> ngay để đánh giá sản phẩm!</div>
            </div>
        <?php }
        foreach ($comments as $row) { ?>
            <div class="comment" id="comment_<?= $row["comment_id"] ?>">
                <div class="comment-name"><?= $row["fullname"] ?></div>
                <div class="comment-rate"><?php for ($i = 0; $i < $row["rate"]; $i++) echo "★"; ?></div>
                <div class="comment-text"><?= $row["comment"] ?></div>
                <div class="comment-time"><?= $row["time"] ?></div>
                <?php if ($row["reply"]) { ?>
                    <div class="comment-reply">
                        <div class="comment-name"><?= $row["username"] ?></div>
                        <div class="comment-text"><?= $row["reply"] ?></div>
                        <div class="comment-time"><?= $row["reply_time"] ?></div>
                    </div>
                <?php } else if (isset($_SESSION["admin"])) { ?>
                    <div class="comment-reply">
                        <form method="post">
                            <textarea name="reply_text" rows="3" placeholder="Phản hồi bình luận"
                                      required></textarea>
                            <button class="button" type="submit" name="reply"
                                    value="<?= $row["comment_id"] ?>">Trả lời
                            </button>
                        </form>
                    </div>
                <?php } ?>
            </div>
        <?php } ?>
    </div>

    <div class="card transparent">
        <div class="category-name">Các sản phẩm khác</div>
        <?php foreach ($sanphamkhac as $row) { ?>
            <div class="product">
                <a href="product_detail.php?id=<?= $row["product_id"] ?>">
                    <img class="product-image" src="<?= $row["image"] ?>">
                    <div class="product-name"><?= $row["product_name"] ?></div>
                    <div class="product-price">
                        <?= number_format($row["price"], 0, 3, '.') ?>₫
                    </div>
                </a>
            </div>
        <?php } ?>
    </div>

<?php include "footer.php" ?>
