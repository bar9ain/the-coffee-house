<?php
$homepage = true;
include "header.php";

?>
    </div>
    <section class="store">
        <div class="page-content">
            <div class="store-header">
                Cửa hàng
            </div>
            <div class="store-body">
                <div class="store-body-half">
                    <div class="store-title">THE COFFEE HOUSE SIGNATURE</div>
                    <div class="store-content">Với những nghệ nhân rang tâm huyết và đội ngũ Barista tài năng cùng những
                        câu chuyện cà phê đầy cảm hứng, ngôi nhà Signature 85 Nguyễn Văn Cừ, TP. Vinh là không
                        gian dành riêng cho những ai trót yêu say đắm hương vị của những hạt cà phê tuyệt hảo.
                    </div>
                </div>
                <div class="store-body-half">
                    <img class="store-image" src="images/master.jpg">
                </div>
            </div>
            <div class="store-footer">
                <div class="store-footer-item">
                    <div class="store-footer-item-image">
                        <img src="images/p1.jpg">
                    </div>
                    <div class="store-footer-item-text">
                        85 Nguyễn Văn Cừ
                    </div>
                </div>
                <div class="store-footer-item">
                    <div class="store-footer-item-image">
                        <img src="images/p2.jpg">
                    </div>
                    <div class="store-footer-item-text">
                        1 Núi Thành
                    </div>
                </div>
                <div class="store-footer-item">
                    <div class="store-footer-item-image">
                        <img src="images/p3.jpg">
                    </div>
                    <div class="store-footer-item-text">
                        2-4 Cầu Đất
                    </div>
                </div>
                <div class="store-footer-item">
                    <div class="store-footer-item-image">
                        <img src="images/p4.jpg">
                    </div>
                    <div class="store-footer-item-text">
                        98 Huyền Quang
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="about">
        <div class="page-content">
            <div class="about-header">3 GIÁ TRỊ LÀM NÊN THE COFFEE HOUSE</div>
            <div class="about-body">
                <div class="about-row">
                    <div class="about-image">
                        <img src="images/a1.jpg">
                    </div>
                    <div class="about-image">
                        <img src="images/a2.jpg">
                    </div>
                    <div class="about-paragraph">
                        <div class="about-title">TRAO GỬI HẠNH PHÚC</div>
                        <div class="about-content">Mọi quyết định và hành động ở The Coffee House đều bắt đầu từ sứ mệnh
                            “Deliver Happiness” - Trao gửi hạnh phúc. Từ niềm vui cho nhân viên đến sự hài lòng của
                            khách hàng, chúng tôi tin rằng tất cả mọi người đều có thể đóng góp thêm những việc làm tốt
                            đẹp cho cộng đồng. Hạnh phúc được tạo ra và lan tỏa, với The Coffee House, mới là hạnh phúc
                            trọn vẹn.
                        </div>
                    </div>
                </div>
                <div class="about-row">
                    <div class="about-paragraph">
                        <div class="about-title">CHẤT LƯỢNG CÀ PHÊ TUYỆT HẢO</div>
                        <div class="about-content">Chúng tôi muốn tạo ra dấu ấn khác biệt cho cà phê Việt Nam bằng sự tử
                            tế và cẩn trọng. Chúng tôi ươm mầm, chăm dưỡng kỹ lưỡng cây cà phê dưới các chuẩn mực khắt
                            khe để đưa ra thị trường những thành phẩm tuyệt vời nhất. Chúng tôi muốn góp phần tạo nên
                            những thay đổi bền vững cho ngành cà phê Việt Nam.
                        </div>
                    </div>
                    <div class="about-image">
                        <img src="images/a3.jpg">
                    </div>
                </div>
                <div class="about-row">
                    <div class="about-image">
                        <img src="images/a4.jpg">
                    </div>
                    <div class="about-paragraph">
                        <div class="about-title">TRÂN TRỌNG CON NGƯỜI</div>
                        <div class="about-content">The Coffee House không chỉ là một công ty hoạt động trong ngành F&B
                            (Food & Beverage - Ăn Uống), chúng tôi là một thương hiệu về con người.
                            Tại The Coffee House, chúng tôi không đi tìm thứ cà phê đặc sản mà tìm những người làm cà
                            phê trở nên đặc biệt. Chúng tôi cùng nhau làm việc cần mẫn và chung sức cho những mục tiêu
                            lớn, nhưng vẫn chăm chút đến từng chi tiết nhỏ. Cùng nhau bình tĩnh vượt qua các thách thức
                            và vươn đến sự hoàn hảo, chúng tôi tin rằng mỗi người đều có một câu chuyện, biệt tài, tiềm
                            năng... nên luôn cổ vũ cho từng cá nhân kiên trì đi đến tận cùng của ước mơ của mình.
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="squad">
        <div class="page-content">
            <div class="about-header">NHỮNG NGƯỜI TẠO NÊN SỰ ĐẶC BIỆT</div>
            <div class="about-body">
                <div class="about-row">
                    <div class="about-column">
                        <div class="about-image">
                            <img src="images/squad/s1.jpg">
                        </div>
                        <div class="about-row">
                            <div class="about-column">
                                <div class="about-image">
                                    <img src="images/squad/s2.jpg">
                                </div>
                            </div>
                            <div class="about-column">
                                <div class="about-image">
                                    <img src="images/squad/s4.jpg">
                                </div>
                                <div class="about-image">
                                    <img src="images/squad/s3.jpg">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="about-column">
                        <div class="about-image">
                            <img src="images/squad/s5.jpg">
                        </div>
                        <div class="about-image">
                            <img src="images/squad/s6.jpg">
                        </div>
                        <div class="about-image">
                            <img src="images/squad/s7.jpg">
                        </div>
                    </div>
                    <div class="about-column">
                        <div class="about-image">
                            <img src="images/squad/s9.jpg">
                        </div>
                        <div class="about-image">
                            <img src="images/squad/s10.jpg">
                        </div>
                        <div class="about-image">
                            <img src="images/squad/s8.jpg">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="page-content">

<?php include "footer.php" ?>
