<?php include "header.php";

if (isset($_POST["category_id"])) {
    $category_id = $_POST["category_id"];
    $category_name = $_POST["category_name"];
    $sql = "update categories 
            set category_name='$category_name'
            where category_id='$category_id'";
    if ($db->query($sql)) {
        header("Location: category_list.php");
    } else {
        echo $sql;
    }
}
if (isset($_GET["id"])) {
    $category_id = $_GET["id"];
    $sql = "select * from categories where category_id='$category_id'";
    $result = $db->query($sql);
    $row = $result->fetch_assoc();
    if ($row == null)
        header("Location: category_list.php");
} else {
    header("Location: category_list.php");
}

?>

    <main class="main">
        <form method="post">
            <div class="container-fluid">
                <div class="row justify-content-center">
                    <div class="col-md-6">
                        <div class="card">
                            <div class="card-header">
                                Sửa loại sản phẩm
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label for="name">Mã danh mục</label>
                                            <input class="form-control" name="category_id"
                                                   value="<?= $row["category_id"] ?>"
                                                   required readonly>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label for="name">Tên danh mục</label>
                                            <input class="form-control" name="category_name"
                                                   value="<?= $row["category_name"] ?>"
                                                   required>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <button class="btn btn-success" type="submit">Xác nhận</button>
                                <a href="category_list.php" class="btn btn-danger">Trở Lại</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </main>

<?php include "footer.php"; ?>