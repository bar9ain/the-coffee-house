</div>
</div>
<script>
    document.getElementById("user-button").addEventListener("click", function (event) {
        event.preventDefault();
        event.stopPropagation();
        document.getElementById("dropdown").style.display = "unset";
    });

    document.onclick = function (e) {
        document.getElementById("dropdown").style.display = "none";
    };
</script>
<?php

if (isset($alert)) {
    echo "<script>";
    echo "alert('$alert');";
    echo "</script>";
}
if (isset($redirect)) {
    echo "<script>";
    echo "window.location.href = '$redirect'";
    echo "</script>";
}
?>
</body>
</html>