<?php include "header.php";

if (isset($_POST["category_name"])) {
    $category_name = $_POST["category_name"];
    $sql = "INSERT INTO categories (
                category_name
            ) VALUES (
                '$category_name'
            )";
    if ($db->query($sql)) {
        header("Location: category_list.php");
    } else {
        echo $db->error;
    }
}

?>

<main class="main">
    <form method="post">
        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-md-6">
                    <div class="card">
                        <div class="card-header">
                            Thêm loại sản phẩm
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="name">Tên danh mục</label>
                                        <input class="form-control" name="category_name" required>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <button class="btn btn-success" type="submit">Xác nhận</button>
                            <a href="category_list.php" class="btn btn-danger">Trở Lại</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</main>