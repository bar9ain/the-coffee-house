<?php include "header.php";
$sql = "SELECT
            orders.order_id,
            orders.total_amount,
            orders.order_date,
            orders.status,
            customers.fullname
        FROM orders
        LEFT JOIN customers ON orders.customer_id = customers.customer_id
        ORDER BY status, order_date DESC";
$query = $db->query($sql);
$list = array();
while ($row = $query->fetch_array()) {
    $list[] = $row;
}
?>
<main class="main">
    <div class="container-fluid">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    Danh sách đơn hàng
                </div>
                <div class="card-body">
                    <table class="table">
                        <tr>
                            <th>Mã đơn hàng</th>
                            <th>Khách hàng</th>
                            <th>Tổng tiền</th>
                            <th>Thời gian</th>
                            <th>Trạng thái</th>
                            <th>Chức năng</th>
                        </tr>
                        <?php foreach ($list as $item) { ?>
                            <tr>
                                <td>#<?= $item["order_id"] ?></td>
                                <td><?= $item["fullname"] ?></td>
                                <td><?= number_format($item["total_amount"]) ?>đ</td>
                                <td><?= $item["order_date"] ?></td>
                                <td>
                                    <?php
                                    switch ($item["status"]) {
                                        case 0:
                                            echo "Mới";
                                            break;
                                        case 1:
                                            echo "Đã xác nhận";
                                            break;
                                        case 2:
                                            echo "Đang giao hàng";
                                            break;
                                        case 3:
                                            echo "Thành công";
                                            break;
                                        case 4:
                                            echo "Đã hủy";
                                            break;
                                    }
                                    ?>
                                </td>
                                <td>
                                    <a href="order_detail.php?id=<?= $item["order_id"] ?>">Chi tiết</a>
                                </td>
                            </tr>
                        <?php } ?>
                    </table>
                </div>
            </div>
        </div>
    </div>
</main>
<?php include "footer.php" ?>
