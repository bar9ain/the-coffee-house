<?php include "../csdl.php";
if (session_status() === PHP_SESSION_NONE)
    session_start();

if (isset($_SESSION["admin"])) {
    header("Location: admin_list.php");
}

if (isset($_POST["login"])) {
    $tendangnhap = $_POST["tendangnhap"];
    $matkhau = $_POST["matkhau"];
    $sql = "SELECT username
            FROM users
            WHERE username='$tendangnhap'
            AND password='$matkhau'";
    $check = $db->query($sql);
    if ($check->num_rows > 0) {
        $_SESSION["admin"] = $tendangnhap;
        header("Location: admin_list.php");
    } else {
        $login_error = true;
    }
}

?>
<head>
    <title>Admin</title>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="../css/font-awesome.css"/>
    <link rel="stylesheet" type="text/css" href="../css/admin.css"/>
    <link rel="icon" href="../images/logo.png">
</head>

<body class="app flex-row align-items-center  pace-done">

<div class="container" style="margin-top: 100px">
    <form method="post">
        <div class="row justify-content-center">
            <div class="col-md-6">
                <div class="card-group">
                    <div class="card p-4">
                        <div class="card-body">
                            <h1>Đăng nhập</h1>
                            <?php if (isset($login_error)) { ?>
                                <p class="text-danger">Tài khoản hoặc mật khẩu không đúng</p>
                            <?php } ?>
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <i class="fa fa-user"></i>
                                    </span>
                                </div>
                                <input class="form-control" type="text" name="tendangnhap" placeholder="Tên đăng nhập">
                            </div>
                            <div class="input-group mb-4">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <i class="fa fa-lock"></i>
                                    </span>
                                </div>
                                <input class="form-control" type="password" name="matkhau" placeholder="Mật khẩu">
                            </div>
                            <div class="row">
                                <div class="col-6">
                                    <button class="btn btn-primary px-4" type="submit" name="login">Đăng nhập</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
</body>