<?php
if (session_status() === PHP_SESSION_NONE)
    session_start();
include "../csdl.php";

if (!isset($_SESSION["admin"])) {
    header("Location: index.php");
}

// Lấy số comment mới
$sql = "select count(1) as number from comments where seen = 0";
$query = $db->query($sql);
$comments = $query->fetch_assoc()["number"];

// Lấy số đơn hàng mới
$sql = "select count(1) as number from orders where status = 0";
$query = $db->query($sql);
$orders = $query->fetch_assoc()["number"];
?>

<head>
    <title>Admin</title>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" href="../css/font-awesome.css"/>
    <link rel="stylesheet" type="text/css" href="../css/admin.css"/>
    <link rel="icon" href="../images/logo.png">
    <script language="javascript" src="ckeditor/ckeditor.js"></script>
</head>

<body class="app">
<div id="main-wrapper" data-layout="vertical" data-sidebartype="full">
    <header class="topbar">
        <nav class="navbar">
            <div class="navbar-header" data-logobg="skin5">
                <a class="navbar-brand" href="index.php">
                    <b class="logo-icon">
                        <img width="100" src="../images/logo_white.png" alt="homepage" class="light-logo">
                    </b>
                </a>
            </div>
            <div class="expand"></div>
            <div class="navbar-collapse">
                <ul class="navbar-nav">
                    <li class="nav-item dropdown">
                        <a class="dropdown-toggle text-muted" id="user-button">
                            <img src="../images/1.jpg" class="rounded-circle" width="31">
                        </a>
                        <div class="dropdown-menu dropdown-menu-right user-dd animated" id="dropdown">
                            <a class="dropdown-item" href="password.php"><i class="ti-user m-r-5 m-l-5"></i> Đổi mật
                                khẩu</a>
                            <a class="dropdown-item" href="logout.php"><i class="ti-wallet m-r-5 m-l-5"></i> Đăng
                                xuất</a>
                        </div>
                    </li>
                </ul>
            </div>
        </nav>
    </header>

    <aside class="left-sidebar" data-sidebarbg="skin6">
        <div class="scroll-sidebar">
            <nav class="sidebar-nav">
                <ul id="sidebarnav" class="in">
                    <li>
                        <div class="user-profile d-flex no-block dropdown m-t-20">
                            <div class="user-pic"><img src="../images/1.jpg" alt="users" class="rounded-circle"
                                                       width="40"></div>
                            <div class="user-content hide-menu m-l-10">
                                <a href="javascript:void(0)" class="">
                                    <h5 class="m-b-0 user-name font-medium"><?= $_SESSION["admin"] ?></h5>
                                    <span class="op-5 user-email">Administrator</span>
                                </a>
                            </div>
                        </div>
                    </li>

                    <li class="sidebar-item"><a class="sidebar-link" href="admin_list.php">Quản trị viên</a></li>
                    <li class="sidebar-item"><a class="sidebar-link" href="category_list.php">Loại sản phẩm</a></li>
                    <li class="sidebar-item"><a class="sidebar-link" href="product_list.php">Sản phẩm</a></li>
                    <li class="sidebar-item"><a class="sidebar-link" href="order_list.php">
                            Đơn hàng
                            <?php if ($orders > 0) { ?>
                                <span class="badge badge-success ml-1"><?= $orders ?> mới</span>
                            <?php } ?>
                        </a></li>
                    <li class="sidebar-item"><a class="sidebar-link" href="customer_list.php">Khách hàng</a></li>
                    <li class="sidebar-item"><a class="sidebar-link" href="comments.php">
                            Bình luận
                            <?php if ($comments > 0) { ?>
                                <span class="badge badge-success ml-1"><?= $comments ?> mới</span>
                            <?php } ?>
                        </a></li>
                    <li class="sidebar-item"><a class="sidebar-link" href="statistics.php">Thống kê doanh thu</a></li>
                </ul>

            </nav>
        </div>
    </aside>

    <div class="page-wrapper">
