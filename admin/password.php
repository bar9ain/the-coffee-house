<?php
include "header.php";

$tendangnhap = $_SESSION["admin"];

if (isset($_POST["update_password"])) {
    $matkhaucu = $_POST["matkhaucu"];
    $matkhau = $_POST["matkhau"];
    $matkhau2 = $_POST["matkhau2"];
    if ($matkhau !== $matkhau2) {
        $error = "Mật khẩu và mật khẩu xác nhận không giống nhau";
    } else {
        $sql = "SELECT password
                FROM users
                WHERE username = '$tendangnhap'";
        $query = $db->query($sql);
        $result = $query->fetch_assoc();
        if ($matkhaucu !== $result["password"]) {
            $error = "Mật khẩu cũ không chính xác";
        } else {
            $sql = "UPDATE users
                    SET password = '$matkhau'
                    WHERE username = '$tendangnhap'";
            if ($db->query($sql)) {
                $alert = "Mật khẩu mới đã được cập nhật!";
                $redirect = "password.php";
            }
        }
    }
}
?>

<main class="main">
    <form method="post">
        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-md-6">
                    <div class="card">
                        <div class="card-header">
                            Đổi mật khẩu
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <?php if (isset($error)) { ?>
                                        <p class="text-danger"><?= $error ?></p>
                                    <?php } ?>
                                    <div class="form-group">
                                        <label>Mật khẩu hiện tại</label>
                                        <input class="form-control"
                                               type="password"
                                               name="matkhaucu"
                                               placeholder="Mật khẩu hiện tại"
                                               autofocus
                                               required>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label>Mật khẩu mới</label>
                                        <input class="form-control"
                                               type="password"
                                               name="matkhau"
                                               required>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label>Xác nhận mật khẩu</label>
                                        <input class="form-control"
                                               type="password"
                                               name="matkhau2"
                                               required>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <button class="btn btn-success" name="update_password" type="submit">Cập nhật</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</main>
<?php include "footer.php" ?>
