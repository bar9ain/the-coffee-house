<?php include "header.php";

if (isset($_POST["delete"])) {
    $product_id = $_POST["delete"];
    $sql = "delete from products where product_id='$product_id'";
    if ($db->query($sql)) {
        header("Location: product_list.php");
    }
}

$sql = "SELECT * FROM products
        INNER JOIN categories
        ON products.category_id = categories.category_id
        ORDER BY products.created_time DESC";
$query = $db->query($sql);
$list = array();
while ($row = $query->fetch_assoc()) {
    $list[] = $row;
}
?>

    <main class="main">
        <div class="container-fluid">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        Danh mục
                        <a href="product_add.php" class="btn btn-primary pull-right">Thêm mới</a>
                    </div>
                    <div class="card-body">
                        <table class="table">
                            <thead>
                            <tr>
                                <td>Hình ảnh</td>
                                <td>Mã sản phẩm</td>
                                <td>Tên sản phẩm</td>
                                <td>Danh mục</td>
                                <td>Giá</td>
                                <td>Chức năng</td>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($list as $item) { ?>
                                <tr class="admin-table-row">
                                    <td width="100">
                                        <img width="100" src="../<?= $item["image"] ?>" class="product-list-image"></td>
                                    <td width="10%"><?= $item["product_id"] ?></td>
                                    <td><?= $item["product_name"] ?></td>
                                    <td width="300"><?= $item["category_name"] ?></td>
                                    <td width="200"><?= number_format($item["price"]) ?> VNĐ</td>
                                    <td width="150">
                                        <form method="post">
                                            <a href="product_edit.php?id=<?= $item["product_id"] ?>"
                                               class="btn btn-sm btn-primary">Sửa</a>
                                            <button class="btn btn-sm btn-danger" name="delete"
                                                    value="<?= $item["product_id"] ?>">Xóa
                                            </button>
                                        </form>
                                    </td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </main>

<?php include "footer.php"; ?>