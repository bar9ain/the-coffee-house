<?php include "header.php";

if (isset($_POST["approved"])) {
    $id = $_POST["approved"];
    $sql = "update comments set approved=1 where comment_id='$id'";
    if ($db->query($sql)) {
        header("location: comments.php");
    } else echo $db->error;
}

$sql = "select comments.*, customers.email from comments
        left join customers on comments.customer_id = customers.customer_id
        order by comments.time desc";
$query = $db->query($sql);
$list = array();
while ($row = $query->fetch_array()) {
    $list[] = $row;
}
?>
<main class="main">
    <div class="container-fluid">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    Quản lý bình luận
                </div>
                <div class="card-body">
                    <table class="table">
                        <tr>
                            <th>Email</th>
                            <th>Mã sản phẩm</th>
                            <th>Nội dung</th>
                            <th>Thời gian</th>
                            <th>Trạng thái</th>
                            <th>Chức năng</th>
                        </tr>
                        <?php foreach ($list as $item) { ?>
                            <tr>
                                <td><?= $item["email"] ?></td>
                                <td>#<?= $item["product_id"] ?></td>
                                <td><?= $item["comment"] ?></td>
                                <td><?= $item["time"] ?></td>
                                <td>
                                    <?php
                                    switch ($item["seen"]) {
                                        case 0:
                                            echo '<span class="badge badge-success">Mới</span>';
                                            break;
                                        case 1:
                                            echo '<span class="badge badge-dark">Đã xem</span>';
                                            break;
                                        case 2:
                                            echo '<span class="badge badge-orange">Đã phản hồi</span>';
                                            break;
                                    }
                                    ?>
                                </td>
                                <td>
                                    <?php if ($item["approved"] == 1) { ?>
                                        <a href="../product_detail.php?id=<?= $item["product_id"] ?>&comment_id=<?= $item["comment_id"] ?>">
                                            Chi tiết</a>
                                    <?php } else { ?>
                                        <form method="post">
                                            <button class="btn btn-success" name="approved" type="submit"
                                                    value="<?= $item["comment_id"] ?>">Cho phép hiển thị
                                            </button>
                                        </form>
                                    <?php } ?>
                                </td>
                            </tr>
                        <?php } ?>
                    </table>
                </div>
            </div>
        </div>
    </div>
</main>
<?php include "footer.php" ?>
