<?php include "header.php";

if (isset($_POST["delete"])) {
    $category_id = $_POST["delete"];
    $sql = "DELETE FROM categories WHERE category_id='$category_id'";
    $db->query($sql);
    header("Location: category_list.php");
}

$sql = "SELECT * FROM categories ORDER BY category_id";
$query = $db->query($sql);
$list = array();
while ($row = $query->fetch_array()) {
    $list[] = $row;
}
?>

    <main class="main">
        <div class="container-fluid">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        Loại sản phẩm
                        <a href="category_add.php" class="btn btn-primary pull-right">Thêm mới</a>
                    </div>
                    <div class="card-body">
                        <table class="table">
                            <thead>
                            <tr>
                                <th>Mã loại</th>
                                <th>Tên loại</th>
                                <th>Chức năng</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($list as $item) { ?>
                                <tr class="admin-table-row">
                                    <td width="10%"><?= $item["category_id"] ?></td>
                                    <td><?= $item["category_name"] ?></td>
                                    <td width="10%">
                                        <form method="post">
                                            <a href="category_edit.php?id=<?= $item["category_id"] ?>"
                                               class="btn btn-sm btn-primary">Sửa</a>
                                            <button class="btn btn-sm btn-danger" name="delete"
                                                    value="<?= $item["category_id"] ?>">Xóa
                                        </form>
                                    </td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </main>

<?php include "footer.php" ?>