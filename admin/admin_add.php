<?php include "header.php";
if (isset($_POST["admin_add"])) {
    $username = $_POST["username"];
    $password = $_POST["password"];
    $password2 = $_POST["password2"];
    if ($password !== $password2) {
        $register_error = "Mật khẩu không khớp!";
    } else {
        $sql = "INSERT INTO users(
                    username,
                    password
                ) VALUES (
                    '$username',
                    '$password'
                )";
        if ($db->query($sql)) {
            header("location: admin_list.php");
        } else {
            $register_error = "Tên tài khoản đã tồn tại";
        }
    }
}
?>

<main class="main">
    <form method="post">
        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-md-6">
                    <div class="card">
                        <div class="card-header">
                            Thêm quản trị viên
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <?php if (isset($register_error)) { ?>
                                        <p class="text-danger"><?= $register_error ?></p>
                                    <?php } ?>
                                    <div class="form-group">
                                        <label for="name">Tên đăng nhập</label>
                                        <input class="form-control" name="username" required>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label>Mật khẩu</label>
                                        <input class="form-control" type="password" name="password" required>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label>Xác nhận mật khẩu</label>
                                        <input class="form-control" type="password" name="password2" required>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="card-footer">
                            <button class="btn btn-success" type="submit" name="admin_add">Xác nhận</button>
                            <a href="admin_list.php" class="btn btn-danger">Trở Lại</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</main>

<?php include "footer.php" ?>
