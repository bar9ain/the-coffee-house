<?php include "header.php";

if (isset($_POST["delete"])) {
    $username = $_POST["delete"];
    $sql = "DELETE FROM users WHERE username='$username'";
    $db->query($sql);
    header("location: admin_list.php");
}

$admin = $_SESSION["admin"];

$sql = "SELECT username
        FROM users
        WHERE username != '$admin'";
$query = $db->query($sql);
$list = array();
while ($row = $query->fetch_assoc()) {
    $list[] = $row;
}

?>
<main class="main">
    <div class="container-fluid">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    Quản trị viên
                    <a href="admin_add.php" class="btn btn-primary pull-right">Thêm mới</a>
                </div>
                <div class="card-body">
                    <table class="table table-responsive-sm">
                        <thead>
                        <tr>
                            <th>Tên tài khoản</th>
                            <th>Chức năng</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($list as $item) { ?>
                            <tr>
                                <td><?= $item["username"] ?></td>
                                <td width="240px">
                                    <form method="post">
                                        <button class="btn btn-sm btn-danger" name="delete"
                                                value="<?= $item["username"] ?>">Xóa
                                        </button>
                                    </form>
                                </td>
                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</main>
<?php include "footer.php" ?>
