<?php include "header.php";

$list = array();
if (isset($_POST["date"])) {
    $date = $_POST["date"];
    $sql = "SELECT products.product_name, products.price, sum(order_details.quantity) as quantity
            FROM products
            INNER JOIN order_details ON products.product_id = order_details.product_id
            INNER JOIN orders ON order_details.order_id = orders.order_id
            AND month(orders.order_date) = month('$date')
            AND year(orders.order_date) = year('$date')
            AND day(orders.order_date) = day('$date')
            AND orders.status = 3
            GROUP BY products.product_name, products.price";
    $query = $db->query($sql);
    echo $db->error;
    while ($row = $query->fetch_assoc()) {
        $list[] = $row;
    }
}

?>
<main class="main">
    <div class="container-fluid">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    Thống kê doanh thu
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6">
                            <form method="post">
                                <table class="table table-borderless">
                                    <tr>
                                        <th>Theo ngày:</th>
                                        <th></th>
                                    </tr>
                                    <tr>
                                        <td>
                                            <input type="date" class="form-control" name="date"
                                                   value="<?= isset($date) ? $date : null ?>">
                                        </td>
                                        <td>
                                            <button type="submit" class="btn btn-primary">Xem</button>
                                        </td>
                                    </tr>
                                </table>
                            </form>
                        </div>
                    </div>
                    <table class="table">
                        <tr>
                            <th>Sản phẩm</th>
                            <th>Giá</th>
                            <th>Số lượng đã bán</th>
                            <th>Tổng tiền</th>
                        </tr>
                        <?php
                        $sum = 0;
                        foreach ($list as $item) {
                            $sum += $item["price"] * $item["quantity"]; ?>
                            <tr>
                                <td><?= $item["product_name"] ?></td>
                                <td><?= number_format($item["price"]) ?>đ</td>
                                <td><?= $item["quantity"] ?></td>
                                <td><?= number_format($item["price"] * $item["quantity"]) ?>đ</td>
                            </tr>
                        <?php } ?>
                        <tr>
                            <td colspan="3" class="text-right"> Tổng tiền:</td>
                            <td><?= number_format($sum) ?>đ</td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</main>
<?php include "footer.php" ?>
