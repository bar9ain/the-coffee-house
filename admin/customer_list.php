<?php include "header.php";
$sql = "SELECT * FROM customers";
$query = $db->query($sql);
$list = array();
while ($row = $query->fetch_array()) {
    $list[] = $row;
}
?>

<main class="main">
    <div class="container-fluid">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    Khách hàng
                </div>
                <div class="card-body">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>Mã khách hàng</th>
                            <th>Họ tên</th>
                            <th>Địa chỉ</th>
                            <th>Email</th>
                            <th>SĐT</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($list as $item) { ?>
                            <tr class="admin-table-row">
                                <td width="10%"><?= $item["customer_id"] ?></td>
                                <td><?= $item["fullname"] ?></td>
                                <td><?= $item["address"] ?></td>
                                <td><?= $item["email"] ?></td>
                                <td><?= $item["phone"] ?></td>
                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</main>
<?php include "footer.php" ?>
