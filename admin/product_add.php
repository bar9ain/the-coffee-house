<?php include "header.php";

if (isset($_POST["product_name"])) {
    $product_name = $_POST["product_name"];
    $category_id = $_POST["category_id"];
    $price = $_POST["price"];
    $description = $_POST["description"];

    if (isset($_FILES["image"]) && $_FILES["image"]["size"] > 0) {
        $time = time();
        $type = explode("/", $_FILES["image"]["type"])[1];
        $image = "images/$time.$type";
        move_uploaded_file($_FILES["image"]["tmp_name"], "../" . $image);
    }
    $sql = "INSERT INTO products (product_name, category_id, price, image, description)
            VALUES ('$product_name', '$category_id', '$price', '$image', '$description')";
    if ($db->query($sql)) {
        header("Location: product_list.php");
    } else echo $db->error;
}

$sql = "select * from categories";
$query = $db->query($sql);
$list = array();
while ($row = $query->fetch_array()) {
    $list[] = $row;
}
?>

    <main class="main">
        <form method="post" enctype="multipart/form-data">
            <div class="container-fluid">
                <div class="row justify-content-center">
                    <div class="col-md-9">
                        <div class="card">
                            <div class="card-header">
                                Thêm sản phẩm
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label>Tên sản phẩm</label>
                                            <input type="text" name="product_name" class="form-control" required
                                                   autofocus>
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label>Danh mục sản phẩm</label>
                                            <select class="form-control" name="category_id">
                                                <?php foreach ($list as $item) { ?>
                                                    <option value="<?= $item["category_id"] ?>"><?= $item["category_name"] ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label>Giá</label>
                                            <input type="number" name="price" class="form-control" required>
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label>Mô tả sản phẩm</label>
                                            <textarea name="description" class="form-control"></textarea>
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label>Hình ảnh sản phẩm</label>
                                            <input type="file" name="image" accept="image/*" class="form-control"
                                                   required>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <button class="btn btn-success" type="submit">Xác nhận</button>
                                <a href="product_list.php" class="btn btn-danger">Trở Lại</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </main>

    <script>
        CKEDITOR.replace('description');
    </script>

<?php include "footer.php"; ?>