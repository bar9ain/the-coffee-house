<?php
include "header.php";

if (isset($_POST["update"])) {
    $order_id = $_POST["update"];
    $status = $_POST["status"];
    $sql = "UPDATE orders
            SET status = $status
            WHERE order_id = '$order_id'";
    if ($db->query($sql)) {
        header("location: order_list.php");
    } else echo $sql;
}

if (isset($_GET["id"])) {
    $order_id = $_GET["id"];
    $sql = "SELECT
            orders.order_id,
            orders.total_amount,
            orders.order_date,
            orders.status,
            customers.fullname,
            customers.address,
            customers.phone,
            customers.email
        FROM orders
        LEFT JOIN customers ON orders.customer_id = customers.customer_id
        WHERE order_id = '$order_id'";
    $result = $db->query($sql);
    $row = $result->fetch_assoc();
    if ($row == null) {
        header("Location: order_list.php");
    }
    $sql = "SELECT
                products.product_id,
                products.product_name,
                products.image,
                products.price,
                order_details.quantity
            FROM order_details
            LEFT JOIN products ON order_details.product_id = products.product_id
            WHERE order_id = '$order_id'";
    $result = $db->query($sql);
    $list = array();
    while ($i = $result->fetch_array()) {
        $list[] = $i;
    }
} else {
    header("Location: order_list.php");
}
?>

<main class="main">
    <form method="post" enctype="multipart/form-data">
        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-md-9">
                    <div class="card">
                        <div class="card-header">
                            Chi tiết đơn hàng
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-4">
                                    <h4 class="cart-detail-customer"><?= $row["fullname"] ?></h4>
                                    <div class="text-muted"><?= $row["address"] ?></div>
                                    <div class="text-muted"><?= $row["phone"] ?></div>
                                    <div class="text-muted"><?= $row["email"] ?></div>
                                </div>
                                <div class="col-md-8">
                                    <table class="table">
                                        <?php foreach ($list as $item) { ?>
                                            <tr>
                                                <td><img width="80" src="../<?= $item["image"] ?>"></td>
                                                <td>
                                                    <div><?= $item["product_name"] ?></div>
                                                    <div class="text-muted">x<?= $item["quantity"] ?></div>
                                                </td>
                                                <td><?= number_format($item["price"]) ?>đ</td>
                                            </tr>
                                        <?php } ?>
                                    </table>
                                    <table class="table table-bordered">
                                        <tr>
                                            <td>Mã đơn hàng</td>
                                            <td><?= $order_id ?></td>
                                        </tr>
                                        <tr>
                                            <td>Thời gian</td>
                                            <td><?= $row["order_date"] ?></td>
                                        </tr>
                                        <tr>
                                            <td>Tổng tiền</td>
                                            <td class="text-danger"><?= number_format($row["total_amount"]) ?>đ</td>
                                        </tr>
                                        <tr>
                                            <td>Trạng thái</td>
                                            <td>
                                                <select class="form-control" name="status">
                                                    <option value="0" <?= $row["status"] == 0 ? "selected" : "" ?>>Mới</option>
                                                    <option value="1" <?= $row["status"] == 1 ? "selected" : "" ?>>Đã xác nhận</option>
                                                    <option value="2" <?= $row["status"] == 2 ? "selected" : "" ?>>Đang giao hàng</option>
                                                    <option value="3" <?= $row["status"] == 3 ? "selected" : "" ?>>Thành công</option>
                                                    <option value="4" <?= $row["status"] == 4 ? "selected" : "" ?>>Đã hủy</option>
                                                </select>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer text-right">
                            <a href="order_list.php" class="btn btn-primary">Trở lại</a>
                            <button class="btn btn-success" name="update" value="<?= $row["order_id"] ?>">Cập nhật</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</main>

<?php include "footer.php" ?>
