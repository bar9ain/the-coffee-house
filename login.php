<?php include "header.php";

if (isset($_SESSION["customer_id"])) {
    header("Location: index.php");
}

if (isset($_POST["login"])) {
    $tendangnhap = $_POST["tendangnhap"];
    $matkhau = $_POST["matkhau"];
    $sql = "SELECT customer_id, fullname
            FROM customers
            WHERE (email='$tendangnhap' OR phone='$tendangnhap')
            AND password='$matkhau'";
    $check = $db->query($sql);
    if ($check->num_rows > 0) {
        $row = $check->fetch_assoc();
        $customer_id = $row["customer_id"];
        // Đăng xuất session trước đó
        $sql1 = "UPDATE active_sessions SET logged_out = 1 WHERE user_id = '$customer_id'";
        $db->query($sql1);
        // Tạo session mới
        $sql2 = "INSERT INTO active_sessions(user_id) VALUES ('$customer_id')";
        $db->query($sql2);
        $_SESSION["customer_id"] = $customer_id;
        $_SESSION["fullname"] = $row["fullname"];
        header("Location: index.php");
    } else {
        $login_error = true;
    }
}

?>
    <form method="post">
        <div class="authen-modal">
            <div class="authen-header">
                <a class="authen-header-tab active">Đăng nhập</a>
            </div>
            <div class="authen-body">
                <div class="input-with-status">
                    <input class="input-with-status-input"
                           type="text"
                           name="tendangnhap"
                           placeholder="Email/Số điện thoại"
                           autofocus
                           required>
                </div>
                <div class="input-with-status">
                    <input class="input-with-status-input"
                           type="password"
                           name="matkhau"
                           placeholder="Mật khẩu"
                           required>
                </div>
                <?php if (isset($login_error)) { ?>
                    <div class="authen-error">Tài khoản hoặc mật khẩu không đúng</div>
                <?php } ?>
                <div class="forgot-password">
                    <a href="register.php">Đăng ký</a>
                    <a href="forgot_password.php">Quên mật khẩu</a>
                </div>
            </div>
            <div class="authen-footer">
                <a href="index.php" class="btn-cancel">Trở Lại</a>
                <button type="submit" name="login" class="button">
                    Đăng nhập
                </button>
            </div>
        </div>
    </form>
<?php include "footer.php" ?>
