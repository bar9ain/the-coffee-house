<?php
include "header.php";
if (!isset($_SESSION["customer_id"])) {
    header("location: login.php");
}

$customer_id = $_SESSION["customer_id"];

if (isset($_POST["save"])) {
    $_email = $_POST["email"];
    $_phone = $_POST["phone"];
    $_hoten = $_POST["hoten"];
    $_diachi = $_POST["diachi"];
    $sql = "UPDATE customers
            SET fullname = '$_hoten',
                address = '$_diachi',
                email = '$_email',
                phone = '$_phone'
            WHERE customer_id = '$customer_id'";
    if ($db->query($sql)) {
        $_SESSION["fullname"] = $_hoten;
        header("location: profile.php");
    } else {
        $alert = "Email hoặc số điện thoại đã được người khác sử dụng!";
        $redirect = "profile.php";
    }
}

$sql = "SELECT
            customer_id,
            fullname,
            address,
            phone,
            email
        FROM customers
        WHERE customer_id='$customer_id'";
$query = $db->query($sql);
$row = $query->fetch_assoc();
if (isset($row)) {
    $fullname = $row["fullname"];
    $address = $row["address"];
    $phone = $row["phone"];
    $email = $row["email"];
} else {
    header("location: login.php");
}
?>
<form method="post">
    <div class="user-page">
        <?php include "profile_menu.php" ?>
        <div class="user-page-content">
            <div class="user-page-header">
                <div class="user-page-header-title">Hồ sơ của tôi</div>
                <div class="user-page-header-subtitle">Quản lý thông tin hồ sơ để bảo mật tài khoản</div>
            </div>
            <div class="user-page-profile">
                <div class="user-page-profile-left">
                    <div class="input-with-label">
                        <div class="input-with-label-label">Tên đầy đủ</div>
                        <div class="input-with-label-content">
                            <input class="input-with-status-input" name="hoten" value="<?= $fullname ?>" required>
                        </div>
                    </div>
                    <div class="input-with-label">
                        <div class="input-with-label-label">Email</div>
                        <div class="input-with-label-content">
                            <input class="input-with-status-input" type="email" name="email" value="<?= $email ?>"
                                   required>
                        </div>
                    </div>
                    <div class="input-with-label">
                        <div class="input-with-label-label">Số điện thoại</div>
                        <div class="input-with-label-content">
                            <input class="input-with-status-input" name="phone" value="<?= $phone ?>" required>
                        </div>
                    </div>
                    <div class="input-with-label">
                        <div class="input-with-label-label">Địa chỉ</div>
                        <div class="input-with-label-content">
                            <input class="input-with-status-input" name="diachi" value="<?= $address ?>" required>
                        </div>
                    </div>
                    <div class="user-page-submit">
                        <button type="submit" name="save" class="button">Lưu</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
<?php include "footer.php" ?>
