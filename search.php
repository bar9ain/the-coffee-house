<?php include "header.php";

$timkiem = isset($_POST["search"]) ? $_POST["search"] : "";

// Get sản phẩm theo thứ tự mới nhất
$sql = "SELECT
            product_id,
            product_name,
            image,
            price
        FROM products
        WHERE product_name LIKE '%$timkiem%'
        ORDER BY created_time DESC";
$query = $db->query($sql);
$sanpham = array();
while ($row = $query->fetch_array()) {
    $sanpham[] = $row;
}

?>
    <div class="card transparent">
        <div class="category-name">Kết quả tìm kiếm cho '<?= $timkiem ?>'</div>
        <?php foreach ($sanpham as $row) { ?>
            <div class="product">
                <a href="product_detail.php?id=<?= $row["product_id"] ?>">
                    <img class="product-image" src="<?= $row["image"] ?>">
                    <div class="product-name"><?= $row["product_name"] ?></div>
                    <div class="product-price">
                        <?= number_format($row["price"]) ?>đ
                    </div>
                </a>
            </div>
        <?php } ?>
    </div>

<?php include "footer.php" ?>